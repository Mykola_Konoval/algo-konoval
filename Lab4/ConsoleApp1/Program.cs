﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        
            static void Main(string[] args)
            {
                Stack<int> elements = new Stack<int>();
                int choice = 0;
                int str = 0;
                Console.WriteLine("Стек \n" +
                                  "1. Додавання елемента в стек \n" +
                                  "2. Вивід на екран \n" +
                                  "3. Видалити перший в стеку елемент \n" +
                                  "4. Видалення списка ");
                while (true)
                {
                    choice = Int32.Parse(Console.ReadLine());
                    if (choice == 1)
                    {
                        Console.Write("Введіть значення \n");
                        str = Int32.Parse(Console.ReadLine());
                        elements.Push(str);
                    }


                    else if (choice == 2)
                    {
                        foreach (int i in elements)
                        {
                            Console.Write(i + " ");
                        }
                    }
                    else if (choice == 3)
                    {
                        elements.Pop();
                    }
                    else if (choice == 4)
                    {
                        elements.Clear();
                    }
                    else break;
                }

            }
    }

    
}
