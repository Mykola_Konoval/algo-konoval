﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> elements = new Queue<int>();
            int choice = 0;
            int str = 0;
            Console.WriteLine("Очередь \n" +
                              "1. Додавання елемента в кінець черги \n" +
                              "2. Вивід на екран \n" +
                              "3. Видалити елемент в черзі \n" +
                              "4. Видалення списка");
            while (true)
            {
                choice = Int32.Parse(Console.ReadLine());
                if (choice == 1)
                {
                    Console.Write("Введіть значення ");
                    str = Int32.Parse(Console.ReadLine());
                    elements.Enqueue(str);
                }

                else if (choice == 2)
                {
                    Console.WriteLine("Список");
                    foreach (int i in elements)
                    {
                        Console.Write(i + " ");
                    }
                }
                else if (choice == 3)
                {
                    elements.Dequeue();
                }
                else if (choice == 4)
                {
                    elements.Clear();
                    Console.WriteLine("Список очищений");
                }
                else break;
            }

        }
    }
}
