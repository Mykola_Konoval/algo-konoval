﻿// Task4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
union myfloat
{
    struct
    {
        unsigned char first;
        unsigned char second;
        unsigned char third;
        unsigned char fourth;
    } bytes;
    float value;
};
void printBits(unsigned char x)
{
    int b;
    unsigned char arr[8];
    for (int i = 0; i < 8; i++)
    {
        arr[i] = x % 2;
        x /= 2;
    }
    printf(" ");
    for (int i = 7; i >= 0; i--)
        printf("%d", arr[i]);
    printf(" ");

}
int main()
{
    myfloat myfloat;
    printf("float number:");
    scanf_s("%f", &myfloat.value);


    printf("\nbits:");
    printBits(myfloat.bytes.fourth);
    printBits(myfloat.bytes.third);
    printBits(myfloat.bytes.second);
    printBits(myfloat.bytes.first);

    printf("\nbytes: ");
    printf("%d %d %d %d", myfloat.bytes.first, myfloat.bytes.second, myfloat.bytes.third, myfloat.bytes.fourth);

    unsigned char arr[8];
    int x = myfloat.bytes.fourth;

    for (int i = 0; i < 8; i++)
    {
        arr[i] = x % 2;
        x /= 2;
    }

    printf("\nsign: %d", arr[7]);


    unsigned char arr2[8];

    x = myfloat.bytes.third;
    for (int i = 0; i < 8; i++)
    {

        arr2[i] = x % 2;
        x /= 2;
    }

    printf("\nmantisa: ");
    for (int i = 6; i >= 0; i--)
        printf("%d", arr[i]);
    printf("%d", arr[7]);

    printf("\nnumber: ");
    for (int i = 6; i >= 0; i--)
        printf("%d", arr2[i]);

    printBits(myfloat.bytes.second);
    printBits(myfloat.bytes.first);
    printf("\nSizeOf MyType=%d\n", sizeof(myfloat));
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
