﻿#include <stdio.h>
#include <math.h>

int main() {
    int n;
    double result;

    printf("n\tf(n)=n\n");
    for (n = 0; n <= 50; n++) {
        result = n;
        printf("%d)\t%f\n", n, result);
    }


    printf("\nn\tf(n) = log(n)\n");
    for (n = 1; n <= 50; n++) {
        result = log(n);
        printf("%d)\t%f\n", n, result);
    }


    printf("\nn\tf(n) = n*log(n)\n");
    for (n = 1; n <= 50; n++) {
        result = n * log(n);
        printf("%d)\t%f\n", n, result);
    }


    printf("\nn\tf(n) = n^2\n");
    for (n = 0; n <= 50; n++) {
        result = pow(n, 2);
        printf("%d)\t%f\n", n, result);
    }

    printf("\nn\tf(n) = 2^n\n");
    for (n = 0; n <= 50; n++) {
        result = pow(2, n);
        printf("%d)\t%2f\n", n, result);
    }

    printf("\nn\tf(n) = n!\n");
    for (n = 0; n <= 50; n++) {
        result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        printf("%d)\t%f\n", n, result);
    }

    return 0;
}