#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

int maxBinaryNumber(int array[], int m) {
   
    for (int i = 0; i < m; i++) {
        for (int j = i + 1; j < m; j++) {
            if (array[i] < array[j]) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }

    
    int maxNumb = 0;
    for (int i = 0; i < m; i++) {
        maxNumb = maxNumb * 2 + array[i];
    }

    return maxNumb;
}

int main() {
    
    srand(time(0));
    int m;
    
    printf("Input m:");
    scanf_s("%d", &m);
     
    int array[40];
    auto begin = GETTIME();
    for (int i = 0; i < m; i++) {
        array[i] = rand() % 2;  
    }

    
    printf("Number in binary: ");
    for (int i = 0; i < m; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");

    
    int maxNumb = maxBinaryNumber(array, m);
    getchar();
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    
  
    printf("Maximum binary number: %d\n", maxNumb);
    printf("The time: %lld ns\n", elapsed_ns.count());
    return 0;
}

