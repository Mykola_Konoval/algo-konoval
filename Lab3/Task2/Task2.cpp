﻿#include <stdio.h>
#include <chrono>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

int fibonacci(int n) {
    if (n <= 0) { 
        return 0;
    }
    else if (n == 1) { 
        return 1;
    }
    else { 
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
int main()
{
    int n;
    printf("Enter n: ");
    scanf_s("%d", &n);
   

    if (n < 0 || n > 40) {
        printf("Invalid input!\n");
    }
    else {
        auto begin = GETTIME();
        int result = fibonacci(n);
        getchar();
        auto end = GETTIME();
        auto elapsed_ns = CALCTIME(end - begin);
        printf("Fibonacci: %d = %d\n", n, result);
        printf("The time: %lld ns\n", elapsed_ns.count());
       
    }
    return 0;
}

