﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
namespace Lab5
{
    class Program
    {
        static void SortInsertionLinkedList(LinkedList<int> list)
        {
            LinkedListNode<int> node = list.First;
            for (LinkedListNode<int> iNode = node.Next; iNode != null; iNode = iNode.Next)
            {
                int temp = iNode.Value;
                for (LinkedListNode<int> jNode = iNode.Previous; jNode != null && jNode.Value > temp; jNode = jNode.Previous)
                {
                    jNode.Next.Value = jNode.Value;
                    jNode.Value = temp;
                }
            }
        }
        static void Main(string[] args)
        {
            LinkedList<int> list1 = new LinkedList<int>();
            LinkedList<int> list2 = new LinkedList<int>();
            Random rnd = new Random();
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            int size = 100;
            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(100);
                list1.AddLast(random);
                list2.AddLast(random);
            }
            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }
            Console.WriteLine("\n\n");
            SortInsertionLinkedList(list1);
            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }
            stopwatch.Stop();
            Console.WriteLine("Час сортування \n: " + stopwatch.Elapsed);
        }
    }
}
