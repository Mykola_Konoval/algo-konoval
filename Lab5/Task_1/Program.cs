﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Lab5
{
    class Program
    {
        static void SortSelectionLinkedList(LinkedList<int> list)
        {
            for (LinkedListNode<int> iN = list.First; iN != null; iN = iN.Next)
            {
                LinkedListNode<int> minNode = iN;
                for (LinkedListNode<int> jN = iN.Next; jN != null; jN = jN.Next)
                    if (jN.Value < minNode.Value)
                        minNode = jN;
                int temp = iN.Value;
                iN.Value = minNode.Value;
                minNode.Value = temp;
            }
        }
        static void Main(string[] args)
        {
            LinkedList<int> list1 = new LinkedList<int>();
            LinkedList<int> list2 = new LinkedList<int>();
            Random rnd = new Random();
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            int s = 100;
            for (int i = 0; i < s; i++)
            {
                int random = rnd.Next(100);
                list1.AddLast(random);
                list2.AddLast(random);
            }
            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }
            Console.WriteLine("\n\n");
            SortSelectionLinkedList(list1);
            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }
            stopwatch.Stop();
            Console.WriteLine("\n Час сортування: " + stopwatch.Elapsed);
        }
    }
}
