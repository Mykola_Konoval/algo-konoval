﻿#include <iostream>
#include <math.h>
#include <time.h>
#include <windows.h>
long long Random(long long seed = 0)
{
	int a = 1103515245;
	char c = 12345;
	long long m = (1LL << 31);
	static long long x;
	if (seed != 0)
	{
		x = seed;
	}
	x = (a * x + c) % m;
	x = x;
	return x;
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	Random(time(0));
	long long n = 0;
	double mat = 0, disp = 0, p;
	int arr[50000], tmp, c = 0;
	for (int i = 0; i < 50000; i++)
	{
		n = Random() % 300;
		arr[i] = n;
	}
	for (int i = 1; i < 50000; i++)
	{
		tmp = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > tmp; j--)
		{
			arr[j + 1] = arr[j];
			arr[j] = tmp;
		}
	}
	for (int i = 0; i < 50000; i++)
	{
		if (arr[i] == arr[i + 1])
		{
			c++;
		}
		else
		{
			if (i == 0)
			{
				c = 1;
			}
			else
			{
				if (arr[i] != arr[i - 1])
				{
					c = 1;
				}
			}
			p = c / 50000.0;
			printf("Величина: %d; частота появи: %d; статична імовірність:% f\n", arr[i], c, p);
				mat += arr[i] * p;
			c = 0;
		}
	}
	c = 0;
	for (int i = 0; i < 50000; i++)
	{
		if (arr[i] == arr[i + 1])
		{
			c++;
		}
		else
		{
			if (i == 0)
			{
				c = 1;
			}
			else
			{
				if (arr[i] != arr[i - 1])
				{
					c = 1;
				}
			}
			p = c / 50000.0;
			disp += pow((arr[i] - mat), 2) * p;
			c = 0;
		}
	}
	printf("Математичне сподівання = %f\n", mat);
	printf("Дисперсія = %f\n", disp);
	printf("Середньоквадратичне відхилення = %f\n", sqrt(disp));
}